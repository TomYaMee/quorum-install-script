#!/bin/bash
echo "[*] Starting Constellation Nodes"
sudo bash ./constellation-start.sh

echo "[*] Starting Geth Nodes"
ARGS="--nodiscover --raft --rpc --rpcaddr 0.0.0.0 --rpcapi admin,db,eth,debug,miner,net,shh,txpool,personal,web3,quorum --emitcheckpoints"
PRIVATE_CONFIG=qdata/constellation.ipc nohup geth --datadir qdata/node $ARGS --raftport 50402 --rpcport 22001 --port 21001 --unlock 0 --password password.txt 2>>qdata/logs/1.log &

echo "[*] Nodes started. Run geth attach qdata/node/geth.ipc to access the geth console"
