#!/bin/bash
#Create a permission-nodes.json at root dir

mkdir qdata
mkdir qdata/node/
cp istan-static-nodes.json qdata/node/static-nodes.json
cp istan-static-nodes.json qdata/node/permissioned-nodes.json

#Generate Constellation Keynode
#Reference: https://github.com/jpmorganchase/constellation#generating-keys
constellation-node --generatekeys=node
mv node.pub qdata/
mv node.key qdata/

#Generate new keystore/account
geth --datadir qdata/node/ account new --password password.txt
