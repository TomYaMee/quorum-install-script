#!/bin/bash

while :
do
  echo -e "Supported OSes: \n (1) Ubuntu \n (2) Debian \n (3) OpenSUSE \n (4) Fedora \n"
  read -p "Please select your chosen OS: " OS

  if [ "$OS" -eq 1 ]; then
    echo "[*] Installing Dependencies"
    bash ./dep-ubuntu.sh
    break
  elif [ "$OS" -eq 2 ]; then
    echo "[*] Installing Dependencies"
    bash ./dep-debian.sh
    break
  elif [ "$OS" -eq 3 ]; then
    echo "[*] Installing Dependencies"
    bash ./dep-opensuse.sh
    break
  elif [ "$OS" -eq 4 ]; then
    echo "[*] Installing Dependencies"
    bash ./dep-fedora.sh
    break
  else
    echo "Please input only numbers from 1 to 4."
  fi
done

while :
do
  echo -e "Supported Consensus: \n (1) Raft \n (2) Istanbul \n"
  read -p "Please select your chosen consensus: " consensus

  if [ "$consensus" -eq 1 ]; then
    echo "[*] Setting Up Quorum"
    bash ./raft-setup.sh
    break
  elif [ "$consensus" -eq 2 ]; then
    while :
    do
      echo -e "Supported Option: \n (1) Y (Yes) \n (2) N (No) \n"
      read -p "Are you setting the first node in the network?: " option

      if [ "$option" -eq 1 ]; then
        echo "[*] Setting Up First Node"
        bash ./istan-gen.sh
        break
      elif [ "$option" -eq 2 ]; then
        break
      else
        echo "Please input only numbers from 1 to 2."
      fi
    done

    echo "[*] Setting Up Quorum"
    bash ./istan-setup.sh
    break
  else
    echo "Please input only numbers from 1 to 2."
  fi
done

echo "Setup complete. Please configure the genesis file as needed and run ./raft-init.sh to initialize the raft node or ./istan-init.sh to intialize the istanbul node."
