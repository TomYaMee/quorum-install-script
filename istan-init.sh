#!/bin/bash
#Create new genesis.json at root dir (Redirect to configured download link in the future)
#Only run when genesis file is properly configured

mkdir qdata/{keystore,geth}
geth --datadir qdata/node init istan-genesis.json
#cp 0/nodekey qdata/node/geth/nodekey
#rm -rf 0/
mkdir qdata/logs

ARGS="--nodiscover --syncmode full --mine --rpc --rpcaddr 0.0.0.0 --rpcapi admin,db,eth,debug,miner,net,shh,txpool,personal,web3,quorum,istanbul"
nohup geth --datadir qdata/node $ARGS --rpcport 22000 --port 21000 --unlock 0 --password password.txt 2>>qdata/logs/1.log &

sleep 5
sudo killall geth

#Get enodeid from nodekey
nodekey=$(sudo cat qdata/node/geth/nodekey)
enodeid=$(bootnode -nodekeyhex $nodekey -writeaddress)
echo $enodeid >> enodeid.txt
echo "Save the enode ID to be used in permissioned-nodes.json"

echo "[*] Node initialized, run ./istan-start.sh to start the node after configuration."
