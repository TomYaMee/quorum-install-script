sudo apt-get install libtool m4 automake bison flex libpam0g-dev
git clone https://tildeslash@bitbucket.org/tildeslash/monit.git
cd monit
./bootstrap
./configure
make
make install