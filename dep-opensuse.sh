#!/bin/bash
#Install Dependencies
apt-get update
#build-essential equivalent
zypper --non-interactive install --type pattern devel_basis
#libtinfo-dev equivalent
zypper --non-interactive install ncurses-devel
#libdb-dev equivalent
zypper addrepo https://download.opensuse.org/repositories/devel:libraries:c_c++/openSUSE_Leap_15.0/devel:libraries:c_c++.repo
zypper --gpg-auto-import-keys refresh
zypper --non-interactive install libdb-5_3

zypper --non-interactive install unzip leveldb libsodium-devel wrk
mv /usr/lib64/libsodium.so.23 /usr/lib64/libsodium.so.18

#Install solc
zypper addrepo http://download.opensuse.org/repositories/system:/snappy/openSUSE_Leap_42.3/ snappy
zypper --gpg-auto-import-keys refresh
zypper --non-interactive install snapd
systemctl enable --now snapd.socket
snap install solc

# install constellation
CVER="0.3.2"
CREL="constellation-$CVER-ubuntu1604"
wget -q https://github.com/jpmorganchase/constellation/releases/download/v$CVER/$CREL.tar.xz
tar xfJ $CREL.tar.xz
cp $CREL/constellation-node /usr/bin && chmod 0755 /usr/bin/constellation-node
rm -rf $CREL
rm -f $CREL.tar.xz
#get compatible libdb/libtinfo from solc
cp /snap/core/4650/usr/lib/x86_64-linux-gnu/libdb-5.3.so /usr/lib64/libdb-5.3.so
cp /snap/core/4650/lib/x86_64-linux-gnu/libtinfo.so.5 /lib64/libtinfo.so.5

# install golang
GOREL=go1.9.3.linux-amd64.tar.gz
wget -q https://dl.google.com/go/$GOREL
tar xfz $GOREL
cp go /usr/go
mv go /usr/local/go
rm -f $GOREL
rm -rf go/
PATH=$PATH:/usr/go/bin
echo 'PATH=$PATH:/usr/go/bin' >> /home/$SUDO_USER/.bashrc
PATH=$PATH:/usr/local/go/bin
echo 'PATH=$PATH:/usr/local/go/bin' >> /home/$SUDO_USER/.bashrc

# install istanbul-Tools
pwd=$(pwd)
cd
go get github.com/getamis/istanbul-tools/cmd/istanbul
cd go/src/github.com/getamis/istanbul-tools
make
cp build/bin/istanbul /usr/local/bin
cd $pwd

# make/install quorum
git clone https://github.com/jpmorganchase/quorum.git
pushd quorum >/dev/null
git checkout tags/v2.0.1
make all
cp build/bin/geth /usr/bin
cp build/bin/bootnode /usr/bin
popd >/dev/null
rm -rf quorum/

# install Porosity
wget -q https://github.com/jpmorganchase/quorum/releases/download/v1.2.0/porosity
mv porosity /usr/bin && chmod 0755 /usr/bin/porosity
