#!/bin/bash
set -u
set -e
PUBLIC_IP=$(dig +short myip.opendns.com @resolver1.opendns.com 2>/dev/null || curl -s http://whatismyip.akamai.com/)
for i in {1..1}
do
    DDIR="qdata"
    mkdir -p $DDIR
    mkdir -p qdata/logs
    rm -f "$DDIR/constellation.ipc"
    CMD="constellation-node --url=https://$PUBLIC_IP:9002/ --port=9002 --workdir=$DDIR --socket=constellation.ipc --publickeys=node.pub --privatekeys=node.key --othernodes=https://127.0.0.1:9002/"
    echo "$CMD >> qdata/logs/constellation$i.log 2>&1 &"
    $CMD >> "qdata/logs/constellation$i.log" 2>&1 &
done

DOWN=true
while $DOWN;
do
    sleep 0.1
    DOWN=false
    for i in {1..1}
    do
	if [ ! -S "qdata/constellation.ipc" ]; then
            DOWN=true
	fi
    done
done
