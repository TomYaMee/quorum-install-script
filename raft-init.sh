#!/bin/bash
#Create new genesis.json at root dir (Redirect to configured download link in the future)
#Only run when genesis file is properly configured
geth --datadir qdata/node init raft-genesis.json
mkdir qdata/logs

ARGS="--nodiscover --raft --rpc --rpcaddr 0.0.0.0 --rpcapi admin,db,eth,debug,miner,net,shh,txpool,personal,web3,quorum --emitcheckpoints"
nohup geth --datadir qdata/node $ARGS --raftport 50402 --rpcport 22001 --port 21001 --unlock 0 --password password.txt 2>>qdata/logs/1.log &

sleep 5
sudo killall geth

#Get enodeid from nodekey
nodekey=$(sudo cat qdata/node/geth/nodekey)
enodeid=$(bootnode -nodekeyhex $nodekey -writeaddress)
echo $enodeid >> enodeid.txt
echo "Save the enode ID to be used in permissioned-nodes.json"
echo "[*] Node initialized, run ./raft-start.sh to start the node after configuration."
